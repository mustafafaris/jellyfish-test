class CheckBoxesList extends HTMLElement {
  constructor() {
    super();
    this.checkboxList = checkboxList;
  }

  renderCheckboxList = (checkboxList) => {
    const container = document.createElement("ul");

    checkboxList.forEach(checkbox => {
      container.appendChild(this.renderCheckbox(checkbox));
    })

    return container;
  }

  renderCheckbox = (element) => {
    const container = document.createElement("li");
    container.classList.add("checkbox");

    if (element.checked) {
      container.classList.add("checked");
    }

    if (element.children.length > 0) {
      const foldButton = document.createElement("button");
      foldButton.innerHTML = arrowIcon;
      foldButton.onclick = this.toggleFold;
      foldButton.setAttribute("class", "fold-button");
      container.appendChild(foldButton);
    }

    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.setAttribute("id", element.id);
    checkbox.setAttribute("hidden", true);
    checkbox.onchange = this.handleChange;

    if (element.checked) {
      checkbox.setAttribute("checked", "true");
    }

    const box = document.createElement("div");
    box.setAttribute("class", "box");
    box.innerHTML = `${checkIcon} ${minusIcon}`;

    const label = document.createElement("label");
    label.innerText = element.label;
    label.setAttribute("for", element.id);
    label.appendChild(box);

    container.appendChild(checkbox);
    container.appendChild(label);

    if (element.children.length > 0) {
        const children = this.renderCheckboxChildren(element.children);
        container.appendChild(children);
    }
    
    return container;
  }

  renderCheckboxChildren = (childrenElements) => {
    const container = document.createElement("ul");
    container.setAttribute("class", "checkbox-container");

    childrenElements.forEach(element => {
        const checkBox = this.renderCheckbox(element);
        container.appendChild(checkBox);
    })
    return container
  }

  toggleFold = ({ target }) => {
    if (target.parentElement.classList.contains("unfolded")) {
      target.parentElement.classList.remove("unfolded");
    } else {
      target.parentElement.classList.add("unfolded");
    }
  }

  handleChange = ({ target }, skipChildren) => {

    if (target.checked) {
      target.parentElement.classList.add("checked");
    } else {
      target.parentElement.classList.remove("checked");
    }

    if (!skipChildren) {
      const children = target.parentElement.querySelectorAll(".checkbox");
      if (children) {
        children.forEach((child) => {
          if (target.checked) {
            child.classList.add("checked");
          } else {
            child.classList.remove("checked");
          }
          child.querySelector('input').checked = target.checked;
        });
      }
    }

    this.updateParent(target);
  }

  updateParent = (target) => {
    const parent = target.parentElement?.parentElement?.parentElement;
    if (!parent) return;
    const parentCheckbox = parent.querySelector("input");
    const children = [].slice.call(parent.querySelectorAll('.checkbox-container .checkbox'));
    const allChecked = children.every((child) => child.classList.contains('checked'));

    if (allChecked) {
      parentCheckbox.checked = target.checked;
      this.handleChange({target: parentCheckbox})
    } else {
      parentCheckbox.checked = false;
      this.handleChange({target: parentCheckbox}, true)
    }
  }

  connectedCallback() {
    const shadowRoot = this.attachShadow({ mode: "open" });
    const stylesLink = document.createElement('link');
    stylesLink.setAttribute("rel", "stylesheet");
    stylesLink.setAttribute("href", "./css/index.css");
    shadowRoot.appendChild(stylesLink);

    const checkboxesList = this.renderCheckboxList(this.checkboxList);
    shadowRoot.appendChild(checkboxesList);
  }
}

customElements.define("checkbox-list", CheckBoxesList);
