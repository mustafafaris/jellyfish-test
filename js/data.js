const checkboxList = [
  {
    id: crypto.randomUUID(),
    label: "Search engine",
    checked: false,
    children: [],
  },
  {
    id: crypto.randomUUID(),
    label: "france-menuiseries.fr",
    checked: false,
    children: [
      {
        id: crypto.randomUUID(),
        label: "Position",
        checked: false,
        children: [
          {
            id: crypto.randomUUID(),
            label: "11 oct 2021",
            checked: true,
            children: [],
          },
          {
            id: crypto.randomUUID(),
            label: "12 oct 2021",
            checked: false,
            children: [],
          },
          {
            id: crypto.randomUUID(),
            label: "Trends",
            checked: false,
            children: [],
          },
        ],
      },
      {
        id: crypto.randomUUID(),
        label: "Pixel Ranking",
        checked: false,
        children: [
          {
            id: crypto.randomUUID(),
            label: "Foo",
            checked: true,
            children: [],
          },
          {
            id: crypto.randomUUID(),
            label: "Bar",
            checked: false,
            children: [],
          },
        ],
      },
    ],
  },
];
